package com.example.ex2;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;

import com.example.ex2.model.Model;
import com.example.ex2.model.Student;

public class EditStudentFragment extends Fragment {

    EditText nameEt, idEt, phoneEt, addressEt;
    CheckBox cb;
    Student student;
    int pos;

    public static StudentDetailsFragment newInstance(String title) {
        StudentDetailsFragment frag = new StudentDetailsFragment();
        Bundle bundle = new Bundle();
        bundle.putString("pos", title);
        frag.setArguments(bundle);
        return frag;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = getArguments();
        if (bundle != null) {
            this.pos = bundle.getInt("index");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.activity_edit_student, container, false);

        student = Model.instance().getAllStudents().get(pos);

        nameEt = view.findViewById(R.id.main_name_et);
        idEt = view.findViewById(R.id.main_id_et);
        phoneEt = view.findViewById(R.id.main_phone_et);
        addressEt = view.findViewById(R.id.main_address_et);
        cb = view.findViewById(R.id.main_cb);

        nameEt.setText(student.name);
        idEt.setText(student.id);
        phoneEt.setText(student.phone);
        addressEt.setText(student.address);
        cb.setChecked(student.checked);
//        cb.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                int pos = (int)cb.getTag();
//                Student st = data.get(pos);
//                st.checked = cb.isChecked();
//            }
//        });
        Button saveButton = view.findViewById(R.id.main_save_btn);
        Button cancelButton = view.findViewById(R.id.main_cancel_btn);
        Button deleteButton = view.findViewById(R.id.main_delete_btn);

        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                save();
                goToList(view);
            }
        });

        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cacnel(view);
            }
        });

        deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Model.instance().deleteStudent(student.id);
                goToList(view);
            }
        });
        return view;
    }

    public void cacnel(View view){
        NavController navController = Navigation.findNavController(view);
        navController.popBackStack();
    }

    public void goToList(View view) {
        NavController navController = Navigation.findNavController(view);
        navController.popBackStack(R.id.studentsListFragment, false);
    }

    public void save(){
        Log.d("TAG",student.toString());
        student = Model.instance().getAllStudents().get(pos);
        String orginalId = student.id;
        student.name = nameEt.getText().toString();
        student.id = idEt.getText().toString();
        student.phone = phoneEt.getText().toString();
        student.address = addressEt.getText().toString();
        student.checked = cb.isChecked();
        Log.d("TAG",student.toString());

        Model.instance().updateStudent(orginalId,student);
    }
}