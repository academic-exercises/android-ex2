package com.example.ex2;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;

import com.example.ex2.model.Model;
import com.example.ex2.model.Student;

import java.util.List;

public class StudentDetailsFragment extends Fragment {
    List<Student> data;
    TextView nameTv, idTv,phoneTv,addressTv;
    CheckBox cb;
    int pos;

    public static StudentDetailsFragment newInstance(String title){
        StudentDetailsFragment frag = new StudentDetailsFragment();
        Bundle bundle = new Bundle();
        bundle.putString("pos",title);
        frag.setArguments(bundle);
        return frag;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = getArguments();
        if (bundle != null){
            this.pos = bundle.getInt("index");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_student_details, container, false);

        Log.d("TAG", "here");

        Student student = Model.instance().getAllStudents().get(pos);

        nameTv = view.findViewById(R.id.details_name_tv);
        idTv = view.findViewById(R.id.details_id_tv);
        phoneTv = view.findViewById(R.id.details_phone_tv);
        addressTv = view.findViewById(R.id.details_address_tv);
        cb = view.findViewById(R.id.main_cb);

        nameTv.setText(student.name);
        idTv.setText(student.id);
        phoneTv.setText(student.phone);
        addressTv.setText(student.address);
        cb.setChecked(student.checked);
        cb.setEnabled(false);

        Button editBtn = view.findViewById(R.id.main_save_btn);
        editBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int action = R.id.action_studentDetailsFragment_to_editStudentFragment;
                Bundle args = new Bundle();
                args.putInt("index", pos);
                Navigation.findNavController(view).navigate(action, args);
            }
        });

        return view;
    }
}