package com.example.ex2.model;
import android.util.Log;

import java.util.Random;

import java.util.LinkedList;
import java.util.List;

public class Model {
    private static final Model _instance = new Model();

    public static Model instance() {
        return _instance;
    }

    private Model() {
        Random random = new Random();

        for (int i = 0; i < 5; i++) {
            boolean randomBoolean = random.nextBoolean();
            addStudent(new Student("name " + i, "" + i, "123456789", "Tel aviv", randomBoolean));
        }
    }

    List<Student> data = new LinkedList<>();

    public List<Student> getAllStudents() {
        return data;
    }

    public void addStudent(Student st) {
        data.add(st);
    }

    public void deleteStudent(String id) {
        for (Student st : data) {
            if (st.id.equals(id)) {
                data.remove(st);
                return;
            }
        }
    }

    public void updateStudent(String originalId, Student newStudent) {
        for (Student st : data) {
            if (st.id.equals(originalId)) {
                Log.d("TAG", "updateStudent: " + st.id + " " + originalId);
                st.name = newStudent.name;
                st.id = newStudent.id;
                st.phone = newStudent.phone;
                st.address = newStudent.address;
                st.checked = newStudent.checked;
                return;
            }
        }

    }
}

