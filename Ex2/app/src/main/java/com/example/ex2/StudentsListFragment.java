package com.example.ex2;


import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.example.ex2.model.Model;
import com.example.ex2.model.Student;

import java.util.List;

public class StudentsListFragment extends Fragment {
    List<Student> data;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.students_list, container, false);
        data = Model.instance().getAllStudents();
        RecyclerView list = view.findViewById(R.id.student_list_re);
        list.setHasFixedSize(true);

        list.setLayoutManager(new LinearLayoutManager(getContext()));
        StudentRecyclerAdapter adapter = new StudentRecyclerAdapter(getLayoutInflater(),data);
        list.setAdapter(adapter);

        adapter.setOnItemClickListener(new StudentRecyclerAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(int pos) {
                int action = R.id.action_studentsListFragment4_to_studentDetailsFragment;
                Bundle args = new Bundle();
                args.putInt("index", pos);
                Navigation.findNavController(view).navigate(action, args);
            }
        });

        View addButton = view.findViewById(R.id.add_student_btn);
        addButton.setOnClickListener(Navigation.createNavigateOnClickListener(R.id.action_global_newStudentActivity));
        return view;
    }
}